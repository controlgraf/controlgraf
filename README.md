
Controlgraf es una empresa ubicada en Barcelona que se dedica a la industria gráfica. Ofrece una amplia gama de equipos y maquinaria para las artes gráficas, incluyendo retractiladoras. Estas máquinas se utilizan para envolver y proteger paquetes utilizando plástico retráctil. Controlgraf ofrece tanto modelos manuales como automáticos de retractiladoras, adaptándose a diferentes necesidades de producción.

Además de las [retractiladoras](https://www.controlgraf.com/manipulado-impresion-offset-y-digital/maquinas-retractiladoras/retractiladoras-de-palets/), Controlgraf también distribuye otros equipos y recambios para la industria gráfica, como cizallas, guillotinas de papel, enfardadoras de palets y envolvedoras de paquete.

Controlgraf se destaca por ofrecer productos de alta calidad y eficiencia en sus servicios. Cuenta con una amplia experiencia en el sector de las artes gráficas, con más de 35 años en el mercado.
